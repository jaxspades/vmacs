;; Load Settings Files Functions for VMacs

(defun vmacs-load-setting-files (directory file-name-list)
  "Load package files defined in the list from the VMacs settings directory"
  (let (file-path directory-path)
    (dolist (element file-name-list)
      ;; Allow top level setting loading, and sub directories
      (setq directory-path (if (string= "" directory) "" (concat directory "/")))
      (setq file-path (concat "~/.emacs.d/settings/" directory-path element ".el"))
      (if (file-exists-p file-path)
          (load-file file-path)))))
