# vmacs Setup
This is vmacs, or (v)im-e(macs). I wanted my own evil mode emacs config, one
that especially was pretty easy to maneuver, yet feature rich. It's a bit
light on the features, at the moment, but I'm adding more every few weeks.

## Features

* Package management through `use-package`
* Evil Mode, because why should you always be good?
* A `custom-init.el` file, where you can store any sensitive configs that you
don't want in a git repo. It's also great for setting up configs for different
machines.
* A separate folder for any copy and pasted/created lisps in `lisps/`
* Theme - Spolsky with ProFontWindows. Looks great on any monitor!
* Line Numbers! On Everything! For now. Hopefully, I can get this to only happen
for certain major modes. If you have any suggestions, add an issue.
* Ivy, Projectile, Smex, and more for completion ease of use
* Auto-modes for Python, Javascript, HTML, CSS, LESS and Org files

## Planned Features

* Space-based menu system using Which-ley or Hydra, similar to Spacemacs
* Integration with the Language Server Protocol for better language features
* Icons for great good
* Cleaner modeline...the default telephone modeline I have is only ok
* Debugging! Maybe! Depends upon how good it can look and how easy it is to set up
* Indentation lines, less pop-ups, and a splash screen! Every config needs that, right? 

## Installation
* First, clone this repo into your `~/.emacs.d` folder.
* Second, download ProFontWindows [here](http://www.fontsquirrel.com/fonts/profontwindows) or change the config to use a different font.

You should now be able to use my config for emacs!

## Organization

* lisp: Folder where you can place custom lisp files which will be loaded by init.el.
* settings: Folder for `editor.el`, which are my 'standard' settings for the editor,
and `custom-editor.el`, which is a great place to store settings for the editor that
git will not override, so any customizations you want or environment specific settings
should go here. Settings also contains a folder for language based settings files,
`languages`, and a folder for package based settings files, `packages`.
* init.el: Main config file; if you want to protect yourself from git overriding your
changes, then I suggest using `settings/custom-editor.el` or `lisp/*.el`. If you're
forking this repo and won't be reintroducing changes later, then edit away. :D

## License
Standard MIT License, so do whatever. I'm not responsible for what happens
after your copy and pasting shenanigans or git cloning or whatever....ing. :D
