;; Smart Modeline configuration -*- lexical-binding: t; -*-

(use-package smart-mode-line
  :ensure t
  :config
  (setq sml/theme 'dark)
  (sml/setup)
  ;; Add the time and file path to the mode line
  (display-time-mode 1))

