;; Projectile and Helm Projectile configuration -*- lexical-binding: t; -*-
(use-package projectile
  :ensure t
  :config
  (projectile-global-mode)
  (defhydra hydra-projectile ()
    "Projectile Menu"
    ("a" projectile-add-known-project "projectile-add-known-project: Add a new project")
    ("f" projectile-find-file "projectile-find-file: Find a file in a project")
    ("p" projectile-switch-project "projectile-switch-project: Switch projects"))
  (define-key my-leader-map "p" 'hydra-projectile/body)
  :init
  (setq projectile-indexing-method 'alien)
  (setq projectile-completion-system 'ivy)
  (setq projectile-enable-caching t))

