;; Built-in Emacs Buffer configuration -*- lexical-binding: t; -*-

;;Make better buffer names when files have the same name
(require 'uniquify)

(defhydra hydra-buffer ()
    "Buffer Menu"
    ("b" ivy-switch-buffer "ivy-switch-buffer: Show the buffer list")
    ("k" kill-buffer "kill-buffer: Kill the selected buffer"))
    (define-key my-leader-map "b" 'hydra-buffer/body)
