;; Visual Basic Editor Settings -*- lexical-binding: t; -*-

(add-to-list 'auto-mode-alist '("\\.vb\\'" . vbnet-mode))
