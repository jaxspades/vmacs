;; Javascript Editor Settings -*- lexical-binding: t; -*-

(use-package js2-mode
  :ensure t
  :config (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode)))
